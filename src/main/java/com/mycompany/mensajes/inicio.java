/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mensajes;

import java.sql.Connection;
import java.util.Scanner;


/**
 *
 * @author BD
 */
public class inicio {
    public static void main(String[] args){
        
        Scanner sc = new Scanner(System.in);
        
        int opc=0;
        do{
            System.out.println("------------------------------------");
            System.out.println("APLICACIÓN DE MENSAJES");
            System.out.println("1. CREAR UN MENSAJE");
            System.out.println("2. LISTAR MENSAJES");
            System.out.println("3. ELIMINAR MENSAJES");
            System.out.println("4. EDITAR MENSAJES");
            System.out.println("5. SALIR");
            
            //LEEMOS LA OPCIÓN SELECCIONADA
            opc = sc.nextInt();
            switch(opc){
                case 1:
                    mensajesService.crearMensaje();
                    break;
                case 2:
                    mensajesService.listarMensaje();
                    break;
                case 3:
                    mensajesService.borrarMensaje();
                    break;
                case 4:
                    mensajesService.editarMensaje();
                    break;  
                default:
                    break;
            }
        }while(opc !=5);
        
    }
}
