/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mensajes;

/**
 *
 * @author BD
 */
public class Mensajes {

    /**
     * se crean las variables de los datos de nuestra tabla
     */
    int id_mensajes;
    String mensaje;
    String autor_mensaje;
    String fecha_mensaje;

    /**
     * Procedemos a crear los constructores el primero que nos sirve por defecto
     * y el segundo para poder enviar todos los datos a nuestra base de datos
     */
    public Mensajes(){
        
    }

    public Mensajes(String mensaje, String autor_mensaje, String fecha_mensaje) {
        this.mensaje = mensaje;
        this.autor_mensaje = autor_mensaje;
        this.fecha_mensaje = fecha_mensaje;
    }
        
    /**
     * para crear los getter and setter se le da click derecho, luego a la
     * opción de insertar codigo y por ultimo en getter and setter
     */
    public int getId_mensajes() {
        return id_mensajes;
    }

    public void setId_mensajes(int id_mensajes) {
        this.id_mensajes = id_mensajes;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getAutor_mensaje() {
        return autor_mensaje;
    }

    public void setAutor_mensaje(String autor_mensaje) {
        this.autor_mensaje = autor_mensaje;
    }

    public String getFecha_mensaje() {
        return fecha_mensaje;
    }

    public void setFecha_mensaje(String fecha_mensaje) {
        this.fecha_mensaje = fecha_mensaje;
    }

}
